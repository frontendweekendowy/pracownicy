import { PracownicyPage } from './app.po';

describe('pracownicy App', () => {
  let page: PracownicyPage;

  beforeEach(() => {
    page = new PracownicyPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
