export class Pracownik {
    id: number;
    imie: string;
    nazwisko: string;
    wiek: number;
    stanowisko: string;
    plec: string;
}