import { Pracownik } from './pracownik';

export const PRACOWNICY: Pracownik[] = [
    {
      id: 1,
      imie: 'Jan',
      nazwisko: 'Kowalski',
      wiek: 32,
      stanowisko: 'CEO',
      plec: 'm'
    },
    {
      id: 2,
      imie: 'Stefan',
      nazwisko: 'Kowalski',
      wiek: 18,
      stanowisko: 'Junior Frontend Developer',
      plec: 'm'
    },
    {
      id: 3,
      imie: 'Angela',
      nazwisko: 'Kowalsky',
      wiek: 21,
      stanowisko: 'Sekretarka',
      plec: 'k'
    },
    {
      id: 4,
      imie: 'Wiesław',
      nazwisko: 'Kowalski',
      wiek: 58,
      stanowisko: 'Konserwator powierzchni płaskich',
      plec: 'm'
    },
    {
      id: 5,
      imie: 'Grazyna',
      nazwisko: 'Kowalska',
      wiek: 55,
      stanowisko: 'Księgowa',
      plec: 'k'
    },
    {
      id: 6,
      imie: 'Natalia',
      nazwisko: 'Kowalska',
      wiek: 22,
      stanowisko: 'Graficzka',
      plec: 'k'
    }
];