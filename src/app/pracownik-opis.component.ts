import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router'

import { Pracownik } from './pracownik';
import { PracownikService } from './pracownik.service';

@Component({
    selector: 'pracownik-opis',
    templateUrl: './pracownik-opis.component.html',
    styleUrls: ['./pracownik-opis.component.css']
})
export class PracownikOpis implements OnInit {
    pracownik: Pracownik;

    constructor(
        private pracownikService: PracownikService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    goBack(): void {
        this.router.navigate(['/dashboard']);
    }

    ngOnInit(): void {
        this.activatedRoute.paramMap
            .switchMap( (params: ParamMap) => {
                return this.pracownikService.getPracownik( +params.get('id') )
            })
            .subscribe( (pracownik) => {
                this.pracownik = pracownik;
            });
    }
}