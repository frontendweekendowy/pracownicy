import { Injectable } from '@angular/core';

import { Pracownik } from './pracownik';
import { PRACOWNICY } from './lista';

@Injectable()
export class PracownikService {
    getPracownicy(): Promise<Pracownik[]> {
        return Promise.resolve(PRACOWNICY);
    };
    getPracownicySlow(): Promise<Pracownik[]> {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve( this.getPracownicy() );
            }, 1000);
        });
    };
    getPracownik(id: number): Promise<Pracownik> {
        return this.getPracownicy().then(
            // function (pracownicy) {
            //     pracownicy.find(function (p) {
            //         p.id === id;
            //     })
            // }
            pracownicy => {
                return pracownicy.find(p => p.id === id);
            }
        );
    };
}