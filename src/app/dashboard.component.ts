import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Pracownik } from './pracownik';
import { PracownikService } from './pracownik.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  pracownik: Pracownik;
  pracownicy: Pracownik[];

  constructor(
    private pracownikService: PracownikService,
    private router: Router
  ) { }

  ustawPracownika(p: Pracownik): void {
    this.pracownik = p;
  }

  goToPracownik(p: Pracownik): void {
    this.router.navigate(['/pracownik', p.id]);
  }

  getPracownicy(): void {
    this.pracownikService.getPracownicy().then((p) => {
      this.pracownicy = p;
    });
  }

  ngOnInit(): void {
    this.getPracownicy();
  }
}
