import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PracownikOpis } from './pracownik-opis.component';
import { DashboardComponent } from './dashboard.component';
import { OnasComponent } from './onas.component';

import { RoutingModule } from './routing.module';

import { PracownikService } from './pracownik.service';

@NgModule({
  declarations: [
    AppComponent,
    PracownikOpis,
    DashboardComponent,
    OnasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule
  ],
  providers: [PracownikService],
  bootstrap: [AppComponent]
})
export class AppModule { }
